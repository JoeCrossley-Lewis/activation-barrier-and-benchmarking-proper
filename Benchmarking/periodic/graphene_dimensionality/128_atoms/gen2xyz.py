#!/usr/bin/env python3

from ase import io
#change the jobname to the name if the file you want to change
atoms = io.read('128_graphene.gen')
atoms.write('128_graphene.xyz', format='xyz')
