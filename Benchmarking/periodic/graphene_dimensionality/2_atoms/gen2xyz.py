#!/usr/bin/env python3

from ase import io
#change the jobname to the name if the file you want to change
atoms = io.read('1.primitive_graphene.gen')
atoms.write('1.primitive_graphene', format='xyz')
