#!/usr/bin/env python3

from ase import io
#change the jobname to the name if the file you want to change
atoms = io.read('5.16x16x1.gen')
atoms.write('5.16x16x1.xyz', format='xyz')
