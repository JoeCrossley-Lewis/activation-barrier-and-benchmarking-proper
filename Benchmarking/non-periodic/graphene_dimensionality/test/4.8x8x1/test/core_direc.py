#!/usr/bin/env python3

import os

#pathname = 1
#directory = "C:\/home/joe/Calculate_activation_barrier/Benchmarking/non-periodic/graphene_dimensionality/4.8x8x1\\" + str(pathname)
#directory = "/home/joe/Calculate_activation_barrier/Benchmarking/non-periodic/graphene_dimensionality/4.8x8x1" + str(pathname)
#while pathname < 3:
#    if not os.path.exists(directory):
#        os.makedirs(directory)
#    pathname += 1

root_path = '/home/joe/Calculate_activation_barrier/Benchmarking/non-periodic/graphene_dimensionality/4.8x8x1/test'
folders = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52']
for folder in folders:
    os.mkdir(os.path.join(root_path, folder))
