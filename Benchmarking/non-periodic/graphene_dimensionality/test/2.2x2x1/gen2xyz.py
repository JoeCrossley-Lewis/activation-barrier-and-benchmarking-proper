#!/usr/bin/env python3

from ase import io
#change the jobname to the name if the file you want to change
atoms = io.read('2.2x2x1.gen')
atoms.write('2.2x2x1.xyz', format='xyz')
