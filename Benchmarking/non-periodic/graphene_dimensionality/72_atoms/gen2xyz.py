#!/usr/bin/env python3

from ase import io
#change the jobname to the name if the file you want to change
atoms = io.read('32_graphene.gen')
atoms.write('32_graphene.xyz', format='xyz')
