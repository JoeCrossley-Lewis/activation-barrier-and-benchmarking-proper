#!/usr/bin/env python3

from matplotlib import pyplot as plt
from matplotlib import ticker
X, Y = [], []
for line in open('values.txt', 'r'):
    values = [float(s) for s in line.split()]
    X.append(values[0])
    Y.append(values[1])
plt.ylabel('Gibbs free energy / kJ mol^-1')
plt.xlabel('Geometry steps')
plt.plot(X, Y)

#Next two lines formate the Y axis correctly so you don't have an OffSet
ax = plt.gca()
ax.ticklabel_format(axis='Y', style = 'plain', useOffset=False)
#plt.show()
plt.savefig('ActivationBarrier.png')
