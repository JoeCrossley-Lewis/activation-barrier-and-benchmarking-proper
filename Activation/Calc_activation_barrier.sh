
#Fetch the gibbs free energy from the output
grep -o 'Gibbs free energy including KE: .*H' output > gibbs_energy.txt
#Fetch just the number for the Gibbs free energy from the gibbs_energy.txt
grep -Eo '\-[0-9\.]+' gibbs_energy.txt > Hartree_energy.txt
#Convert the Gibbs free energy from Hartrees to kJmol^-1 (kJmol^-1 is s.i units for Gibbs energy)
numfmt --from-unit=2600 < Hartree_energy.txt > kjmol_energy.txt
#Fetch the geometry steps from the outpu
grep -o 'Geometry step: .*' output > geom_step.txt
#Fetch just the Geometry Step number
grep -Eo '[0-9\.]+' geom_step.txt > step.txt
#Combine the Geometry steps with the Gibbs free energy associated in one file
paste step.txt kjmol_energy.txt | column -s $'\t' -t > values.txt

mkdir results
mv *.txt results/

mv plot.py results/
cd results/
#execute the python script to make the graph
./plot.py

mv plot.py ../

